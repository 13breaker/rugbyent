package Rugby;

import java.util.ArrayList;
import java.util.Collection;

public class Partido {
	//atributos
	private Integer puntosLocal;
	private Integer puntosVisitante;
	private Integer bonusLocal;
	private Integer bonusVisitante;
	
	//relaciones
	private Jornada jornada;
	private Collection arbitrado = new ArrayList<Arbitro>();
	private Collection seJuega = new ArrayList<Estadio>();
	private Collection visitante = new ArrayList<Equipo>();
	private Collection local = new ArrayList<Equipo>();

	//constructores
	
	//metodos

	public Integer getPuntosLocal() {
		return puntosLocal;
	}

	public void setPuntosLocal(Integer puntosLocal) {
		this.puntosLocal = puntosLocal;
	}

	public Integer getPuntosVisitante() {
		return puntosVisitante;
	}

	public void setPuntosVisitante(Integer puntosVisitante) {
		this.puntosVisitante = puntosVisitante;
	}

	public Integer getBonusLocal() {
		return bonusLocal;
	}

	public void setBonusLocal(Integer bonusLocal) {
		this.bonusLocal = bonusLocal;
	}

	public Integer getBonusVisitante() {
		return bonusVisitante;
	}

	public void setBonusVisitante(Integer bonusVisitante) {
		this.bonusVisitante = bonusVisitante;
	}
	//Valores que devuelve la tabla (void)
	public void resultado() {
		
	}
	
	
}
