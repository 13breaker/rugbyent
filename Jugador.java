package Rugby;

import java.util.ArrayList;
import java.util.Collection;

enum Posicion{
	Pilier,
	Talonador,
	Segunda_linea,
	Tercera_linea,
	Medio_mele,
	Apertura,
	Centro,
	Ala,
	Zaguero;
	
}
public class Jugador extends Persona{
	//atributos
	private String posicion;
	
	//relaciones
	private Collection juega = new ArrayList<Equipo>();
	public java.util.Collection capitan = new java.util.Treeset();
	
	//metodos
	public String getPosicion() {
		return posicion;
	}

	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}
	
	
}
