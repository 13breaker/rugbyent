package Rugby;

import java.util.ArrayList;
import java.util.Collection;

enum Pais{
	Escocia,
	Francia,
	Gales,
	Inglaterra,
	Irlanda,
	Italia;
}
public class Equipo {
	
	//atributos
	private String nombre;

	//relaciones
	private Collection juegaVisitante = new ArrayList<Partido>();
	private Collection juegaLocal = new ArrayList<Partido>();
	private Collection seEnfrentan = new ArrayList<Estadio>();
	private Collection entrenado = new ArrayList<Entrenador>();
	private Collection jugadores = new ArrayList<Jugador>();

	//metodos
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
