package Rugby;

import java.util.ArrayList;
import java.util.Collection;

public class Estadio {
	
	//atributos
	private String nombre;
	private Integer capacidad;
	private String ciudad;
	
	//relaciones
	private Collection seCelebran = new ArrayList<Partido>();
	private Collection juegan = new ArrayList<Equipo>();
	
	//metodos
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(Integer capacidad) {
		this.capacidad = capacidad;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	
}
